package com.carloscardoso.themoviedb.modules.detail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.carloscardoso.themoviedb.mocked_data.movieDetailSuccessResponse
import com.carloscardoso.themoviedb.repository.RemoteMovieRepository
import com.carloscardoso.themoviedb.ui.model.MovieDetailUIModel
import com.carloscardoso.themoviedb.ui.model.UIState
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class MovieDetailViewModelTest {

    private val dispatcher = UnconfinedTestDispatcher()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var viewModel: MovieDetailViewModel
    private val remoteRepo = mockk<RemoteMovieRepository>(relaxed = true)

    // -
    private val uiStateObserver = mockk<Observer<UIState<MovieDetailUIModel>>>(relaxed = true)
    private val trailerUrlObserver = mockk<Observer<String>>(relaxed = true)

    /** */
    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        Dispatchers.setMain(dispatcher)

        viewModel = MovieDetailViewModel(remoteRepo)

        viewModel.uiState.observeForever(uiStateObserver)
        viewModel.trailerVideoUrl.observeForever(trailerUrlObserver)
    }

    @Test
    fun `When get movie detail is a sucessful operation, then ui-state is notified with the detail'`() {
        val mockedId = "01"
        // given
        coEvery { remoteRepo.getMovieDetail(any()) } returns movieDetailSuccessResponse

        // when
        viewModel.fetchMovieDetail(mockedId)

        // then
        val slotUIStateList = mutableListOf<UIState<MovieDetailUIModel>>()
        verify { uiStateObserver.onChanged(capture(slotUIStateList)) }

        val successStateCaptured = slotUIStateList[0] as UIState.Success
        verify(exactly = 1) { uiStateObserver.onChanged(successStateCaptured) }
    }

    /** */
    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

}