package com.carloscardoso.themoviedb.modules.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.carloscardoso.themoviedb.mocked_data.moviesGenericErrorResponse
import com.carloscardoso.themoviedb.mocked_data.moviesNetworkErrorResponse
import com.carloscardoso.themoviedb.mocked_data.moviesSuccessResponse
import com.carloscardoso.themoviedb.repository.LocalMovieRepository
import com.carloscardoso.themoviedb.repository.RemoteMovieRepository
import com.carloscardoso.themoviedb.ui.model.HomeDataModel
import com.carloscardoso.themoviedb.ui.model.MovieListType
import com.carloscardoso.themoviedb.ui.model.UIState
import io.mockk.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class HomeViewModelTest {

    private val dispatcher = UnconfinedTestDispatcher()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var viewModel: HomeViewModel
    private val remoteRepo = mockk<RemoteMovieRepository>(relaxed = true)
    private val localRepo = mockk<LocalMovieRepository>(relaxed = true)

    // -
    private val uiStateObserver = mockk<Observer<UIState<HomeDataModel>>>(relaxed = true)

    /** */
    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        Dispatchers.setMain(dispatcher)

        viewModel = HomeViewModel(remoteRepo, localRepo)

        viewModel.uiState.observeForever(uiStateObserver)
    }

    @Test
    fun `When fetching latest movies from remote is successful, then the MovieListType of the movie list is 'Latest'`() {
        // given
        coEvery { remoteRepo.getLatest() } returns moviesSuccessResponse

        // when
        viewModel.fetchMovies()

        // then
        val slotUIStateList = mutableListOf<UIState<HomeDataModel>>()

        verify { uiStateObserver.onChanged(capture(slotUIStateList)) }

        val loadingStateCaptured = slotUIStateList[0] as UIState.Loading
        val successStateCaptured = slotUIStateList[1] as UIState.Success

        assert(loadingStateCaptured.isLoading)
        assertEquals(successStateCaptured.data.listType, MovieListType.Latest)
    }

    @Test
    fun `When fetching top rated movies from remote is successful, then the MovieListType of the movie list is 'TopRated'`() {
        // given
        coEvery { remoteRepo.getTopRated() } returns moviesSuccessResponse

        // when
        viewModel.fetchMovies()

        // then
        val slotUIStateList = mutableListOf<UIState<HomeDataModel>>()

        verify { uiStateObserver.onChanged(capture(slotUIStateList)) }

        val loadingStateCaptured = slotUIStateList[0] as UIState.Loading
        val successStateCaptured = slotUIStateList[2] as UIState.Success

        assert(loadingStateCaptured.isLoading)
        assertEquals(successStateCaptured.data.listType, MovieListType.TopRated)
    }

    @Test
    fun `When fetching recommended movies from remote is successful, then the MovieListType of the movie list is 'Recommended'`() {
        // given
        coEvery { remoteRepo.getRecommendationsByMovieId(any()) } returns moviesSuccessResponse

        // when
        viewModel.fetchMovies()

        // then
        val slotUIStateList = mutableListOf<UIState<HomeDataModel>>()

        verify { uiStateObserver.onChanged(capture(slotUIStateList)) }

        val loadingStateCaptured = slotUIStateList[0] as UIState.Loading
        val successStateCaptured = slotUIStateList[3] as UIState.Success

        assert(loadingStateCaptured.isLoading)
        assertEquals(successStateCaptured.data.listType, MovieListType.Recommendation)
    }

    @Test
    fun `When there is an error fetching latest movies from remote, then get the movies from local storage`() {
        // given
        coEvery { remoteRepo.getLatest() } answers {
            moviesGenericErrorResponse
            moviesNetworkErrorResponse
        }

        // when
        viewModel.fetchMovies()

        // then
        val slotUIStateList = mutableListOf<UIState<HomeDataModel>>()
        verify { uiStateObserver.onChanged(capture(slotUIStateList)) }

        coVerify(exactly = 1) { localRepo.getLatestMovies() }
    }

    @Test
    fun `When there is an error fetching top rated movies from remote, then get the movies from local storage`() {
        // given
        coEvery { remoteRepo.getTopRated() } answers {
            moviesGenericErrorResponse
            moviesNetworkErrorResponse
        }

        // when
        viewModel.fetchMovies()

        // then
        val slotUIStateList = mutableListOf<UIState<HomeDataModel>>()
        verify { uiStateObserver.onChanged(capture(slotUIStateList)) }

        coVerify(exactly = 1) { localRepo.getRatedMovies() }
    }

    @Test
    fun `When there is an error fetching recomende movies from remote, then get the movies from local storage`() {
        // given
        coEvery { remoteRepo.getRecommendationsByMovieId(any()) } answers {
            moviesGenericErrorResponse
            moviesNetworkErrorResponse
        }

        // when
        viewModel.fetchMovies()

        // then
        val slotUIStateList = mutableListOf<UIState<HomeDataModel>>()
        verify { uiStateObserver.onChanged(capture(slotUIStateList)) }

        coVerify(exactly = 1) { localRepo.getRecommendedMovies() }
    }

    @Test
    fun `When fetching latest movies from remote is successful, then the movies are stored in local database`() {
        // given
        val numOfMovies = moviesSuccessResponse.data.movies?.size ?: 0
        coEvery { remoteRepo.getLatest() } returns moviesSuccessResponse

        // when
        viewModel.fetchMovies()

        // then
        val slotUIStateList = mutableListOf<UIState<HomeDataModel>>()
        verify { uiStateObserver.onChanged(capture(slotUIStateList)) }

        coVerify(exactly = numOfMovies) { localRepo.insertMovie(any()) }
    }

    @Test
    fun `When fetching top rated movies from remote is successful, then the movies are stored in local database`() {
        // given
        val numOfMovies = moviesSuccessResponse.data.movies?.size ?: 0
        coEvery { remoteRepo.getTopRated() } returns moviesSuccessResponse

        // when
        viewModel.fetchMovies()

        // then
        val slotUIStateList = mutableListOf<UIState<HomeDataModel>>()
        verify { uiStateObserver.onChanged(capture(slotUIStateList)) }

        coVerify(exactly = numOfMovies) { localRepo.insertMovie(any()) }
    }

    /** */
    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

}