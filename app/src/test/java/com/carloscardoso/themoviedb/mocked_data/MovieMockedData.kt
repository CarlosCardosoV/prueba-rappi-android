package com.carloscardoso.themoviedb.mocked_data

import com.carloscardoso.themoviedb.network.NetworkCall
import com.carloscardoso.themoviedb.network.model.Dates
import com.carloscardoso.themoviedb.network.model.GetMoviesResponse
import com.carloscardoso.themoviedb.network.model.MovieNetworkModel
import java.util.*

val mockedMovie = MovieNetworkModel(
    adult = false,
    backdropPath = "/qTkJ6kbTeSjqfHCFCmWnfWZJOtm.jpg",
    genreIds = listOf(10751, 16, 12, 35, 14),
    id = 438148,
    originalLanguage = "en",
    originalTitle = "Minions: The Rise of Gru",
    overview = "A fanboy of a supervillain supergroup known as the Vicious 6, Gru hatches a plan to become evil enough to join them, with the backup of his followers, the Minions.",
    popularity = 11846.631,
    posterPath = "/wKiOkZTN9lUUUNZLmtnwubZYONg.jpg",
    releaseDate = Date(),
    title = "Minions: The Rise of Gru",
    video = true,
    voteAverage = 7.6,
    voteCount = 354,
    productionCompanies = emptyList(),
    productionCountries = emptyList(),
    revenue = 0,
    runtime = 0,
    spokenLanguages = emptyList(),
    status = "",
    tagline = ""
)

val responseMocked = GetMoviesResponse(
    dates = Dates(
        maximum = "2022-07-22",
        minimum = "2022-06-04"
    ),
    page = 1,
    totalPages = 68,
    totalResults = 1343,
    movies = listOf(mockedMovie, mockedMovie)
)

val movieDetailSuccessResponse: NetworkCall.Response.Success<MovieNetworkModel> =
    NetworkCall.Response.Success(
        data = mockedMovie
    )


val moviesSuccessResponse: NetworkCall.Response.Success<GetMoviesResponse> =
    NetworkCall.Response.Success(
        data = responseMocked
    )

val moviesGenericErrorResponse: NetworkCall.Response<GetMoviesResponse> =
    NetworkCall.Response.GenericError(code = 100, error = "Generic Error")

val moviesNetworkErrorResponse: NetworkCall.Response<GetMoviesResponse> =
    NetworkCall.Response.NetworkError