package com.carloscardoso.themoviedb.persistence.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.carloscardoso.themoviedb.persistence.model.MovieRoomModel


@Dao
interface MoviesDao {

    @Query("SELECT * FROM movies WHERE type = 'Latest' ")
    suspend fun getLatest(): List<MovieRoomModel>

    @Query("SELECT * FROM movies WHERE type = 'Recommendation' ")
    suspend fun getRecommended(): List<MovieRoomModel>

    @Query("SELECT * FROM movies WHERE type = 'TopRated' ")
    suspend fun getTopRated(): List<MovieRoomModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovie(movie: MovieRoomModel)


}