package com.carloscardoso.themoviedb.persistence.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*


@Entity(tableName = "movies")
data class MovieRoomModel(
    @PrimaryKey var id: Int? = null,
    var mDBId: String? = null,
    val title: String? = null,
    val overview: String? = null,
    val posterPath: String? = null,
    val backdropPath: String? = null,
    val language: String? = null,
    val releaseDate: Date? = null,
    val voteAverage: Double? = null,
    var type: String? = null
)