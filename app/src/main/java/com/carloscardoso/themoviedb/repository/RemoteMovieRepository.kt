package com.carloscardoso.themoviedb.repository

import com.carloscardoso.themoviedb.network.NetworkCall
import com.carloscardoso.themoviedb.network.NetworkCall.Response
import com.carloscardoso.themoviedb.network.api.MovieApiService
import com.carloscardoso.themoviedb.network.model.GetMoviesResponse
import com.carloscardoso.themoviedb.network.model.GetVideosResponse
import com.carloscardoso.themoviedb.network.model.MovieNetworkModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

/** */
interface RemoteMovieRepository {
    suspend fun getLatest(): Response<GetMoviesResponse>
    suspend fun getTopRated(): Response<GetMoviesResponse>
    suspend fun getVideosFromMovie(movieId: String): Response<GetVideosResponse>
    suspend fun getRecommendationsByMovieId(movieId: String): Response<GetMoviesResponse>
    suspend fun getMovieDetail(movieId: String): Response<MovieNetworkModel>
}

class RemoteMovieRepositoryImpl(
    private val networkCall: NetworkCall,
    private val movieApiService: MovieApiService
) : RemoteMovieRepository {

    /** */
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO

    /** */
    override suspend fun getLatest() = networkCall.performCall(dispatcher) {
        movieApiService.getLatest()
    }

    /** */
    override suspend fun getTopRated() = networkCall.performCall(dispatcher) {
        movieApiService.getTopRated()
    }

    /** */
    override suspend fun getRecommendationsByMovieId(movieId: String) =
        networkCall.performCall(dispatcher) {
            movieApiService.getRecommendations(movieId = movieId)
        }

    /** */
    override suspend fun getMovieDetail(movieId: String) = networkCall.performCall(dispatcher) {
        movieApiService.getMovieDetail(movieId)
    }


    /** */
    override suspend fun getVideosFromMovie(movieId: String) = networkCall.performCall(dispatcher) {
        movieApiService.getVideosFromMovie(movieId)
    }


}

