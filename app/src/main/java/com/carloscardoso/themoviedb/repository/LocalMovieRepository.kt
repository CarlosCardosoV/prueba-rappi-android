package com.carloscardoso.themoviedb.repository

import com.carloscardoso.themoviedb.mapper.toUIModel
import com.carloscardoso.themoviedb.persistence.model.MovieRoomModel
import com.carloscardoso.themoviedb.persistence.room.MovieDatabase
import com.carloscardoso.themoviedb.ui.model.MovieUIModel


interface LocalMovieRepository {
    suspend fun getLatestMovies(): List<MovieUIModel>?
    suspend fun getRatedMovies(): List<MovieUIModel>?
    suspend fun getRecommendedMovies(): List<MovieUIModel>?

    suspend fun insertMovie(movie: MovieRoomModel)
}

class LocalMovieRepositoryImpl(private val db: MovieDatabase) : LocalMovieRepository {

    /** */
    override suspend fun getLatestMovies(): List<MovieUIModel> =
        db.movieDao().getLatest().toUIModel()

    /** */
    override suspend fun getRecommendedMovies(): List<MovieUIModel> =
        db.movieDao().getRecommended().toUIModel()

    /** */
    override suspend fun getRatedMovies(): List<MovieUIModel> =
        db.movieDao().getTopRated().toUIModel()

    /** */
    override suspend fun insertMovie(movie: MovieRoomModel) {
        db.movieDao().insertMovie(movie)
    }
}