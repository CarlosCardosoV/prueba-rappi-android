package com.carloscardoso.themoviedb.ui.model

import androidx.annotation.StringRes

sealed class UIState<out T> {
    data class Success<out T>(val data: T) : UIState<T>()
    class Loading(val isLoading: Boolean) : UIState<Nothing>()
    class Error(@StringRes val errorMessageId: Int) : UIState<Nothing>()
    object NetworkError : UIState<Nothing>()
}