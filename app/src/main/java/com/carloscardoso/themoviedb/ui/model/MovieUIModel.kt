package com.carloscardoso.themoviedb.ui.model

import com.carloscardoso.themoviedb.util.year
import java.io.Serializable
import java.text.DecimalFormat
import java.util.*


data class MovieUIModel(
    val id: String,
    val title: String,
    val overview: String,
    val posterPath: String,
    val backdropPath: String,
    val language: String,
    val releaseDate: Date,
    val voteAverage: Double
) : Serializable {
    private val formatter = DecimalFormat("#0.0")

    val year = releaseDate.year().toString()
    val votes = formatter.format(voteAverage)

    // - images (poster and backdrop)
    private val IMAGE_URL = "https://image.tmdb.org/t/p"

    fun getPosterUrl(size: String = "400") = "$IMAGE_URL/w$size/$posterPath"
    fun getBackDropUrl(size: String = "500") = "$IMAGE_URL/w$size/$posterPath"

}