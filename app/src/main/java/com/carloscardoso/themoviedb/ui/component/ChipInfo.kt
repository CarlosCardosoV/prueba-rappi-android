package com.carloscardoso.themoviedb.ui.component

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import androidx.cardview.widget.CardView
import com.airbnb.paris.extensions.style
import com.carloscardoso.themoviedb.R
import com.carloscardoso.themoviedb.databinding.LayoutChipInfoBinding

class ChipInfo @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : CardView(context, attrs, defStyleAttr) {

    private var binding =
        LayoutChipInfoBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        val typedArray =
            context.obtainStyledAttributes(attrs, R.styleable.ChipInfo, defStyleAttr, 0)
        setAttributes(typedArray)
    }


    /** */
    private fun setAttributes(attributes: TypedArray) {
        val bgColor = attributes.getColor(
            R.styleable.ChipInfo_chip_color,
            context.getColor(R.color.gray_timberwolf)
        )

        val icon = attributes.getResourceId(R.styleable.ChipInfo_chip_icon, 0)
        val chipText = attributes.getString(R.styleable.ChipInfo_chip_text)
        val textStyle = ChipTextStyle.values()[attributes.getInt(
            R.styleable.ChipInfo_chip_text_style,
            0
        )]

        setBackGroundColor(bgColor)
        if (icon != 0) {
            setIcon(icon)
        }

        if (!chipText.isNullOrEmpty()) {
            setChipInfo(chipText)
        }

        setTextstyle(textStyle)

        attributes.recycle()
    }

    /**
     * Exposed methods
     */
    fun setChipInfo(info: String) {
        binding.textviewInfo.text = info
    }

    fun setBackGroundColor(@ColorInt color: Int) {
        binding.container.setBackgroundColor(color)

    }

    fun setIcon(@DrawableRes iconRes: Int) {
        with(binding.imageViewIcon) {
            visibility = View.VISIBLE
            setImageResource(iconRes)
        }
    }

    fun setTextstyle(textStyle: ChipTextStyle) {
        with(binding.textviewInfo) {
            when (textStyle) {
                ChipTextStyle.Regular -> style(R.style.chip_regular)
                ChipTextStyle.Bold -> style(R.style.chip_bold)

            }
        }


    }

    enum class ChipTextStyle {
        Regular,
        Bold
    }


}