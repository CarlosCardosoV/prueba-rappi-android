package com.carloscardoso.themoviedb.ui.component.movie_list_component

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.carloscardoso.themoviedb.databinding.ItemMovieBinding
import com.carloscardoso.themoviedb.modules.home.OnMovieClickListener
import com.carloscardoso.themoviedb.ui.model.MovieUIModel
import com.carloscardoso.themoviedb.ui.setHeightPercentage

/** */
class MoviesAdapter(
    private var movies: List<MovieUIModel>,
    private val layoutMode: RecyclerView.LayoutManager,
    private val movieClickListener: OnMovieClickListener
) :
    RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    /** */
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemMovieBinding.inflate(inflater)

        if (layoutMode is GridLayoutManager) {
            changeParamsToGridMode(binding)
        }

        return ViewHolder(binding)
    }

    /** */
    override fun getItemCount() = movies.size

    /** */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder.binding) {
            val movie = movies[position]

            Glide.with(imageViewPoster.context)
                .load(movie.getPosterUrl())
                .into(imageViewPoster)

            imageViewPoster.setOnClickListener { movieClickListener.onMovieClick(movie) }
        }
    }

    /** */
    private fun changeParamsToGridMode(binding: ItemMovieBinding) {
        with(binding) {
            cardMovie.layoutParams.width = RecyclerView.LayoutParams.MATCH_PARENT
            cardMovie.setHeightPercentage(0.35f)
            imageViewPoster.scaleType = ImageView.ScaleType.FIT_XY
        }
    }


    @SuppressLint("NotifyDataSetChanged")
    fun updateData(movies: List<MovieUIModel>) {
        this.movies = movies
        notifyDataSetChanged()
    }

    /** */
    inner class ViewHolder(val binding: ItemMovieBinding) : RecyclerView.ViewHolder(binding.root)

}