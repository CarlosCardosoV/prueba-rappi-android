package com.carloscardoso.themoviedb.ui

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorRes
import androidx.annotation.FloatRange
import androidx.core.content.ContextCompat
import androidx.transition.Fade
import androidx.transition.TransitionManager


fun Context.getColor(@ColorRes color: Int) = ContextCompat.getColor(this, color)

fun View.fadeVisibility(visibility: Int, duration: Long = 400) {
    val transition = Fade().apply {
        this.duration = duration
        addTarget(this@fadeVisibility)

    }

    TransitionManager.beginDelayedTransition(this.parent as ViewGroup, transition)
    this.visibility = visibility
}

fun Context.dp(intValue: Int) = intValue * resources.displayMetrics.density.toInt()

fun View.setWidthPercentage(percentage: Float) {
    val pixels = context.resources.displayMetrics.widthPixels
    layoutParams.width = (pixels * percentage).toInt()
}


fun View.setHeightPercentage(@FloatRange(from = 0.0, to = 1.0) percentage: Float) {
    val pixels = context.resources.displayMetrics.heightPixels
    layoutParams.height = (pixels * percentage).toInt()
}