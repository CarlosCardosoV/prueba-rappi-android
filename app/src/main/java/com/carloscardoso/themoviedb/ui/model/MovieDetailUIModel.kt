package com.carloscardoso.themoviedb.ui.model


data class MovieDetailUIModel(
    val id: Int?,
    val genres: List<String>
) {
    val formattedUIGenres = genres.joinToString(" • ")
}