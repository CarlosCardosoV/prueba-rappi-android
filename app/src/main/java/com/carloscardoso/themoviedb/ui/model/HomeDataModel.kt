package com.carloscardoso.themoviedb.ui.model

/** */
data class HomeDataModel(
    val movies: List<MovieUIModel>? = emptyList(),
    val listType: MovieListType
)

enum class MovieListType {
    Latest,
    TopRated,
    Recommendation
}