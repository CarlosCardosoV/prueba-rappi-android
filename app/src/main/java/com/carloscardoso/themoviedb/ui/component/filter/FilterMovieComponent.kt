package com.carloscardoso.themoviedb.ui.component.filter

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.RelativeLayout
import android.widget.Spinner
import com.airbnb.paris.extensions.style
import com.carloscardoso.themoviedb.R
import com.carloscardoso.themoviedb.databinding.LayoutFilterComponentBinding

/** */
class FilterMovieComponent @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : RelativeLayout(context, attrs, defStyleAttr) {

    private var binding =
        LayoutFilterComponentBinding.inflate(LayoutInflater.from(context), this, true)

    var dataList: List<String> = emptyList()
    var filterText = ""
    var buttonTextNoSelection = ""

    init {
        val typedArray =
            context.obtainStyledAttributes(attrs, R.styleable.FilterMovieComponent, defStyleAttr, 0)
        filterText = typedArray.getString(R.styleable.FilterMovieComponent_filter_text).orEmpty()
        buttonTextNoSelection =
            typedArray.getString(R.styleable.FilterMovieComponent_filter_button_text).orEmpty()

        typedArray.recycle()
    }

    /** */
    fun setFilter(
        dataList: List<String>,
        onItemSelected: (result: String) -> Unit
    ) {
        this.dataList = dataList
        binding.spinner.setFilterData(dataList)
        handleButtonFiltersActions()
        handleSpinnersSelection(onItemSelected)
    }

    /** */
    fun clearFilter() {
        binding.buttonFilter.style(R.style.FilterButtonUnSelected)
        binding.buttonFilter.text = buttonTextNoSelection
    }

    /** */
    private fun Spinner.setFilterData(dataList: List<String>) {
        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
            context,
            android.R.layout.simple_spinner_item, dataList.toTypedArray()
        )

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        this.adapter = adapter
    }

    @SuppressLint("SetTextI18n")
    private fun handleSpinnersSelection(onItemSelected: (result: String) -> Unit) {
        with(binding) {
            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parentView: AdapterView<*>?,
                    selectedItemView: View?,
                    position: Int,
                    id: Long
                ) {
                    buttonFilter.text = "$filterText ${dataList[position]}"
                    buttonFilter.style(R.style.FilterButtonSelected)

                    onItemSelected.invoke(dataList[position])
                }

                override fun onNothingSelected(parentView: AdapterView<*>?) {

                }
            }
        }
    }

    /** */
    private fun handleButtonFiltersActions() {
        with(binding) {
            buttonFilter.setOnClickListener {
                spinner.performClick()
            }
        }
    }


}