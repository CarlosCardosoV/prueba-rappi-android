package com.carloscardoso.themoviedb.ui.component.movie_list_component

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.carloscardoso.themoviedb.databinding.LayoutMovieListComponentBinding
import com.carloscardoso.themoviedb.modules.home.OnMovieClickListener
import com.carloscardoso.themoviedb.ui.fadeVisibility
import com.carloscardoso.themoviedb.ui.model.MovieUIModel


/** */
@SuppressLint("NotifyDataSetChanged")
class MovieUIComponent @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var binding =
        LayoutMovieListComponentBinding.inflate(LayoutInflater.from(context), this, true)

    private val defaultLayoutManager =
        LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

    private var adapter: MoviesAdapter? = null
    private var movies: List<MovieUIModel>? = null

    /**
     * Exposed methods
     */

    /** */
    fun setData(
        headerText: String,
        movies: List<MovieUIModel>?,
        layoutManager: RecyclerView.LayoutManager = defaultLayoutManager,
        takeOnly: Int? = null,
        movieClickListener: OnMovieClickListener
    ) {
        this.movies = movies
        binding.textViewTitle.text = headerText

        val dataToDisplay = if (takeOnly == null) {
            movies
        } else {
            movies?.take(takeOnly)
        }

        dataToDisplay?.let {
            adapter = MoviesAdapter(it, layoutManager, movieClickListener)

            binding.recyclerViewData.scheduleLayoutAnimation()
            binding.recyclerViewData.adapter = adapter
            binding.recyclerViewData.layoutManager = layoutManager
            adapter?.notifyDataSetChanged()
        }
    }

    /** */
    fun setFilters(
        languageList: List<String>,
        yearsList: List<String>
    ) {
        with(binding) {
            containerFilters.fadeVisibility(View.VISIBLE)

            // - language spinner

            filterLanguage.setFilter(languageList) { language ->
                Log.d("eMovie.filterLanguage.item ", language)
                movies?.filter { it.language == language }?.let {
                    adapter?.updateData(it.take(6))
                }

                filterYear.clearFilter()
            }
            filterYear.setFilter(yearsList) { year ->
                Log.d("eMovie.filterYear.item ", year)
                movies?.filter { it.year == year }?.let {
                    adapter?.updateData(it.take(6))
                }

                filterLanguage.clearFilter()
            }
        }
    }


}