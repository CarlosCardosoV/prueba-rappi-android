package com.carloscardoso.themoviedb.network

import com.carloscardoso.themoviedb.BuildConfig
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/** */
object RetrofitClient {

    /** */
    private val CONNECT_TIMEOUT: Long = 100
    private val READ_TIMEOUT: Long = 100
    private val TIME_UNIT = TimeUnit.SECONDS
    private var interceptor = HttpLoggingInterceptor()

    /** */
    private var client = OkHttpClient.Builder()
        .addInterceptor(interceptor.apply {
            this.level = HttpLoggingInterceptor.Level.BODY
        })
        .connectTimeout(CONNECT_TIMEOUT, TIME_UNIT)
        .readTimeout(READ_TIMEOUT, TIME_UNIT)
        .build()

    /** */
    val gson = GsonBuilder()
        .setLenient()
        .create()

    private var retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.API_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(client)
        .build()

    /** */
    fun <S> createService(serviceClass: Class<S>): S {
        return retrofit.create(serviceClass)
    }


}
