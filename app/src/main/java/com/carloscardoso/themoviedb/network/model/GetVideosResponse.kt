package com.carloscardoso.themoviedb.network.model

import com.google.gson.annotations.SerializedName

/** */
data class GetVideosResponse(
    val id: Int?,
    val results: List<Video>?
)

data class Video(
    @SerializedName("id")
    val id: String?,
    @SerializedName("iso_3166_1")
    val iso31661: String?,
    @SerializedName("iso_639_1")
    val iso6391: String?,
    @SerializedName("key")
    val key: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("official")
    val official: Boolean?,
    @SerializedName("published_at")
    val publishedAt: String?,
    @SerializedName("site")
    val site: String?,
    @SerializedName("size")
    val size: Int?,
    @SerializedName("type")
    val type: String?
) {
}

fun List<Video>?.getTrailerFromYoutube(): String? {
    val YOUTUBE_URL = "http://www.youtube.com/watch?v="
    val video = this?.firstOrNull { it.site == "YouTube" && it.type == "Trailer" }

    return if (video != null) {
        YOUTUBE_URL + video.key
    } else {
        null
    }

}