package com.carloscardoso.themoviedb.network.api

import com.carloscardoso.themoviedb.BuildConfig
import com.carloscardoso.themoviedb.network.model.GetMoviesResponse
import com.carloscardoso.themoviedb.network.model.GetVideosResponse
import com.carloscardoso.themoviedb.network.model.MovieNetworkModel
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface MovieApiService {

    @GET("movie/now_playing")
    suspend fun getLatest(
        @Query("api_key") apiKey: String = BuildConfig.TMDBApiKey
    ): GetMoviesResponse

    @GET("movie/top_rated")
    suspend fun getTopRated(
        @Query("api_key") apiKey: String = BuildConfig.TMDBApiKey
    ): GetMoviesResponse

    @GET("movie/{movie_id}/recommendations")
    suspend fun getRecommendations(
        @Path("movie_id") movieId: String,
        @Query("api_key") apiKey: String = BuildConfig.TMDBApiKey
    ): GetMoviesResponse

    @GET("movie/{movie_id}")
    suspend fun getMovieDetail(
        @Path("movie_id") movieId: String,
        @Query("api_key") apiKey: String = BuildConfig.TMDBApiKey
    ): MovieNetworkModel


    @GET("movie/{movie_id}/videos")
    suspend fun getVideosFromMovie(
        @Path("movie_id") movieId: String,
        @Query("api_key") apiKey: String = BuildConfig.TMDBApiKey
    ): GetVideosResponse

}