package com.carloscardoso.themoviedb

import android.app.Application
import com.carloscardoso.themoviedb.di.apiService
import com.carloscardoso.themoviedb.di.repositoryModule
import com.carloscardoso.themoviedb.di.roomModule
import com.carloscardoso.themoviedb.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.module.Module


/** **/
class MovieDbApplication : Application() {

    /** */
    private val appModule: List<Module> =
        viewModelModule + repositoryModule + apiService + roomModule

    /** */
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@MovieDbApplication)
            modules(appModule)
        }
    }
}