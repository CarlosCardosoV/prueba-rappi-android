package com.carloscardoso.themoviedb.mapper

import com.carloscardoso.themoviedb.network.model.MovieNetworkModel
import com.carloscardoso.themoviedb.ui.model.MovieDetailUIModel
import com.carloscardoso.themoviedb.ui.model.MovieUIModel
import java.util.*

/**
 * Map network objects to UI models
 */

// -
fun List<MovieNetworkModel>.toUIModel(): List<MovieUIModel> {
    val movieUiList = mutableListOf<MovieUIModel>()

    this.forEach {
        movieUiList.add(it.toUIModel())
    }

    return movieUiList
}

// -
fun MovieNetworkModel.toUIModel() = MovieUIModel(
    id = id.toString(),
    title = originalTitle.orEmpty(),
    overview = overview.orEmpty(),
    posterPath = posterPath.orEmpty(),
    backdropPath = backdropPath.orEmpty(),
    language = originalLanguage.orEmpty(),
    voteAverage = voteAverage ?: 0.0,
    releaseDate = releaseDate ?: Date()
)

fun MovieNetworkModel.toDetailUIModel() = MovieDetailUIModel(
    id = id,
    genres = genres.map { it.name }

)

