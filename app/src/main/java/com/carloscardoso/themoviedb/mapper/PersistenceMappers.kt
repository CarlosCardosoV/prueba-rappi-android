package com.carloscardoso.themoviedb.mapper

import com.carloscardoso.themoviedb.persistence.model.MovieRoomModel
import com.carloscardoso.themoviedb.ui.model.MovieUIModel
import java.util.*


fun MovieUIModel.toRoomModel() = MovieRoomModel(
    mDBId = id,
    title = title,
    overview = overview,
    posterPath = posterPath,
    backdropPath = backdropPath,
    language = language,
    voteAverage = voteAverage,
    releaseDate = releaseDate
)

fun List<MovieRoomModel>.toUIModel(): List<MovieUIModel> {
    val movieUiList = mutableListOf<MovieUIModel>()

    this.forEach {
        movieUiList.add(it.toUIModel())
    }

    return movieUiList
}

// -
fun MovieRoomModel.toUIModel() = MovieUIModel(
    id = mDBId.toString(),
    title = title.orEmpty(),
    overview = overview.orEmpty(),
    posterPath = posterPath.orEmpty(),
    backdropPath = backdropPath.orEmpty(),
    language = language.orEmpty(),
    voteAverage = voteAverage ?: 0.0,
    releaseDate = releaseDate ?: Date()
)