package com.carloscardoso.themoviedb.util

import java.util.*

fun Date.year(): Int {
    val calendar = Calendar.getInstance()
    calendar.time = this
    return calendar.get(Calendar.YEAR)

}