package com.carloscardoso.themoviedb.di

import com.carloscardoso.themoviedb.modules.detail.MovieDetailViewModel
import com.carloscardoso.themoviedb.modules.home.HomeViewModel
import com.carloscardoso.themoviedb.network.NetworkCall
import com.carloscardoso.themoviedb.network.RetrofitClient
import com.carloscardoso.themoviedb.network.api.MovieApiService
import com.carloscardoso.themoviedb.persistence.room.MovieDatabase
import com.carloscardoso.themoviedb.repository.LocalMovieRepository
import com.carloscardoso.themoviedb.repository.LocalMovieRepositoryImpl
import com.carloscardoso.themoviedb.repository.RemoteMovieRepository
import com.carloscardoso.themoviedb.repository.RemoteMovieRepositoryImpl
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    // -
    viewModel { HomeViewModel(get(), get()) }

    // -
    viewModel { MovieDetailViewModel(get()) }
}

/** */
val repositoryModule = module {
    /** */
    single<RemoteMovieRepository> { RemoteMovieRepositoryImpl(get(), get()) }
    single<LocalMovieRepository> { LocalMovieRepositoryImpl(get()) }
}

val apiService = module {
    single { RetrofitClient.createService(MovieApiService::class.java) }
    single { NetworkCall() }
}

val roomModule = module {
    single { MovieDatabase(androidContext()) }
}
