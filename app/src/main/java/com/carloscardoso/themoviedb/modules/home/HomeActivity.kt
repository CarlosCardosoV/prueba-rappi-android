package com.carloscardoso.themoviedb.modules.home

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.carloscardoso.themoviedb.R
import com.carloscardoso.themoviedb.databinding.ActivityHomeBinding
import com.carloscardoso.themoviedb.modules.base.BaseActivity
import com.carloscardoso.themoviedb.modules.detail.MovieDetailActivity
import com.carloscardoso.themoviedb.modules.home.HomeActivity.Keys.MOVIE_ITEM_KEY
import com.carloscardoso.themoviedb.ui.model.HomeDataModel
import com.carloscardoso.themoviedb.ui.model.MovieListType
import com.carloscardoso.themoviedb.ui.model.MovieUIModel
import com.carloscardoso.themoviedb.ui.model.UIState
import com.carloscardoso.themoviedb.ui.model.UIState.Success
import org.koin.androidx.viewmodel.ext.android.viewModel

/** */
class HomeActivity : BaseActivity(), OnMovieClickListener {

    object Keys {
        const val MOVIE_ITEM_KEY = "movie_item_key"
    }

    /** */
    private val binding by lazy { ActivityHomeBinding.inflate(layoutInflater) }
    private val viewModel: HomeViewModel by viewModel()

    /** */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        // -
        viewModel.fetchMovies()

        observeUiState()
        handleActions()
    }

    /** */
    private fun observeUiState() {
        viewModel.uiState.observe(this) {
            handleUiState(it)
        }
    }

    /** */
    private fun handleUiState(uiState: UIState<HomeDataModel>) {
        when (uiState) {
            is UIState.Error -> {}
            is UIState.Loading -> setLoading(uiState.isLoading)
            is UIState.NetworkError -> {}
            is Success -> setHomeData(uiState.data)

        }
    }

    /** */
    private fun setLoading(isLoading: Boolean) {
        with(binding) {
            swipeRefresh.isRefreshing = isLoading
        }
    }

    /** */
    private fun setHomeData(data: HomeDataModel) {
        when (data.listType) {
            MovieListType.Latest -> {
                binding.movieComponentLatest.setData(
                    headerText = getString(R.string.home_screen_latest_movies_title),
                    movies = data.movies,
                    movieClickListener = this@HomeActivity
                )
            }
            MovieListType.TopRated -> {
                binding.movieComponentTopRated.setData(
                    headerText = getString(R.string.home_screen_top_rated_title),
                    movies = data.movies,
                    movieClickListener = this@HomeActivity
                )
            }
            MovieListType.Recommendation -> {
                binding.movieComponentRecommended.setData(
                    headerText = getString(R.string.home_screen_recommended_title),
                    movies = data.movies,
                    movieClickListener = this@HomeActivity,
                    layoutManager = GridLayoutManager(this@HomeActivity, 2),
                    takeOnly = 6
                )

                val languageList = data.movies?.map { it.language }?.distinct()?.sorted().orEmpty()
                val yearsList =
                    data.movies?.map { it.year }?.distinct()?.sortedDescending().orEmpty()

                binding.movieComponentRecommended.setFilters(
                    languageList = languageList,
                    yearsList = yearsList
                )
            }
        }
    }

    /** */
    private fun handleActions() {
        binding.swipeRefresh.setOnRefreshListener {
            viewModel.fetchMovies()
        }
    }

    /** */
    override fun onMovieClick(movie: MovieUIModel) {
        val intent = Intent(applicationContext, MovieDetailActivity::class.java)
        intent.putExtra(MOVIE_ITEM_KEY, movie)
        startActivity(intent)
    }


}

interface OnMovieClickListener {
    fun onMovieClick(movie: MovieUIModel)
}