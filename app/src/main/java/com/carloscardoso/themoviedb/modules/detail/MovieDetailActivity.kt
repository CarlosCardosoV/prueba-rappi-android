package com.carloscardoso.themoviedb.modules.detail

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.annotation.StringRes
import com.bumptech.glide.Glide
import com.carloscardoso.themoviedb.databinding.ActivityMovieDetailBinding
import com.carloscardoso.themoviedb.modules.base.BaseActivity
import com.carloscardoso.themoviedb.modules.home.HomeActivity.Keys.MOVIE_ITEM_KEY
import com.carloscardoso.themoviedb.ui.fadeVisibility
import com.carloscardoso.themoviedb.ui.model.MovieDetailUIModel
import com.carloscardoso.themoviedb.ui.model.MovieUIModel
import com.carloscardoso.themoviedb.ui.model.UIState
import com.google.android.material.snackbar.Snackbar
import org.koin.androidx.viewmodel.ext.android.viewModel


/** */
class MovieDetailActivity : BaseActivity() {

    /** */
    private val binding by lazy { ActivityMovieDetailBinding.inflate(layoutInflater) }
    private val viewModel: MovieDetailViewModel by viewModel()

    /** */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        val movieItem = intent.getSerializableExtra(MOVIE_ITEM_KEY) as MovieUIModel
        viewModel.fetchMovieDetail(movieItem.id)

        setMovieData(movieItem)
        setViews()
        observeUiState()
        handleActions(movieItem)
    }

    /** */
    private fun setMovieData(movie: MovieUIModel) {
        with(binding) {
            Glide.with(imageViewPoster)
                .load(movie.getPosterUrl())
                .into(imageViewPoster)

            textviewMovieTitle.text = movie.title
            textViewOverView.text = movie.overview

            chipMovieLanguage.setChipInfo(movie.language)
            chipMovieVoteAverage.setChipInfo(movie.votes)
            chipMovieYear.setChipInfo(movie.year)
        }
    }

    /** */
    private fun observeUiState() {
        viewModel.uiState.observe(this) {
            handleUiState(it)
        }

        viewModel.trailerVideoUrl.observe(this) { url ->
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(url)
                )
            )
        }
    }

    /** */
    private fun handleUiState(uiState: UIState<MovieDetailUIModel>) {
        when (uiState) {
            is UIState.Error -> showError(uiState.errorMessageId)
            is UIState.Loading -> setLoading(uiState.isLoading)
            is UIState.NetworkError -> {}
            is UIState.Success -> setMovieDetail(uiState.data)
        }
    }

    /** */
    private fun setMovieDetail(data: MovieDetailUIModel) {
        with(binding) {
            textviewGenres.text = data.formattedUIGenres
            textviewGenres.fadeVisibility(View.VISIBLE)
        }
    }

    private fun showError(@StringRes error: Int) {
        Snackbar.make(
            findViewById(android.R.id.content),
            getString(error),
            Snackbar.LENGTH_LONG
        ).show()

    }

    /** */
    private fun setLoading(isLoading: Boolean) {
        val loaderVisibility = if (isLoading) View.VISIBLE else View.GONE
        binding.progressBar.visibility = loaderVisibility
    }

    /** */
    private fun handleActions(movieItem: MovieUIModel) {
        binding.imageViewPlayTrailer.setOnClickListener {
            viewModel.playTrailer(movieItem.id)
        }
    }


    /** */
    private fun setViews() {
        setUpToolbar(binding.toolbar.root)
    }
}