package com.carloscardoso.themoviedb.modules.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.carloscardoso.themoviedb.R
import com.carloscardoso.themoviedb.mapper.toDetailUIModel
import com.carloscardoso.themoviedb.modules.base.BaseViewModel
import com.carloscardoso.themoviedb.network.NetworkCall.Response
import com.carloscardoso.themoviedb.network.model.Video
import com.carloscardoso.themoviedb.network.model.getTrailerFromYoutube
import com.carloscardoso.themoviedb.repository.RemoteMovieRepository
import com.carloscardoso.themoviedb.ui.model.MovieDetailUIModel
import com.carloscardoso.themoviedb.ui.model.UIState
import kotlinx.coroutines.launch

class MovieDetailViewModel(private val remoteRepository: RemoteMovieRepository) : BaseViewModel() {

    /** */
    private val uiStateMLD = MutableLiveData<UIState<MovieDetailUIModel>>()
    val uiState: LiveData<UIState<MovieDetailUIModel>>
        get() = uiStateMLD

    // -
    private val trailerVideoUrlMLD = MutableLiveData<String>()
    val trailerVideoUrl: LiveData<String>
        get() = trailerVideoUrlMLD


    /** */
    fun fetchMovieDetail(movieId: String) {
        viewModelScope.launch {
            getMovieDetail(movieId)
        }
    }

    /** */
    private suspend fun getMovieDetail(movieId: String) {
        val movieDetailResponse = remoteRepository.getMovieDetail(movieId)

        when (movieDetailResponse) {
            is Response.Success -> {
                val movieDetail = movieDetailResponse.data.toDetailUIModel()
                uiStateMLD.postValue(UIState.Success(movieDetail))
            }
            is Response.GenericError -> {}
            is Response.NetworkError -> {}
        }
    }

    /** */
    fun playTrailer(movieId: String) {
        viewModelScope.launch {
            uiStateMLD.value = UIState.Loading(isLoading = true)

            val videosResponse = remoteRepository.getVideosFromMovie(movieId)
            if (videosResponse is Response.Success) {
                val videosResults = videosResponse.data.results
                handleSuccessVideoResponse(videosResults)
            } else {
                uiStateMLD.value = UIState.Error(R.string.movie_detail_trailer_generic_error)
            }

            uiStateMLD.value = UIState.Loading(isLoading = false)
        }
    }

    /** */
    private fun handleSuccessVideoResponse(videosResults: List<Video>?) {
        if (videosResults?.isEmpty() == false) {
            val trailerFromYoutube = videosResults.getTrailerFromYoutube()

            trailerFromYoutube?.let {
                trailerVideoUrlMLD.postValue(it)
            } ?: kotlin.run {
                uiStateMLD.value = UIState.Error(R.string.movie_detail_trailer_no_video_error)
            }

        } else {
            uiStateMLD.value = UIState.Error(R.string.movie_detail_trailer_no_video_error)
        }
    }


}