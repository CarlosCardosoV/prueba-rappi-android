package com.carloscardoso.themoviedb.modules.base

import android.annotation.SuppressLint
import android.view.MenuItem
import androidx.annotation.ColorRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import com.carloscardoso.themoviedb.R


@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {

    /** */
    fun setUpToolbar(toolbar: Toolbar, @ColorRes resourceColor: Int = R.color.white) {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val upArrow =
            ContextCompat.getDrawable(this, R.drawable.ic_back_arrow)

        upArrow?.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
            resourceColor,
            BlendModeCompat.SCREEN
        )

        supportActionBar?.setHomeAsUpIndicator(upArrow)
    }

    /** */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

}