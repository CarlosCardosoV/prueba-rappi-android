package com.carloscardoso.themoviedb.modules.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.carloscardoso.themoviedb.mapper.toRoomModel
import com.carloscardoso.themoviedb.mapper.toUIModel
import com.carloscardoso.themoviedb.modules.base.BaseViewModel
import com.carloscardoso.themoviedb.network.NetworkCall.Response
import com.carloscardoso.themoviedb.network.model.MovieNetworkModel
import com.carloscardoso.themoviedb.repository.LocalMovieRepository
import com.carloscardoso.themoviedb.repository.RemoteMovieRepository
import com.carloscardoso.themoviedb.ui.model.HomeDataModel
import com.carloscardoso.themoviedb.ui.model.MovieListType
import com.carloscardoso.themoviedb.ui.model.MovieUIModel
import com.carloscardoso.themoviedb.ui.model.UIState
import kotlinx.coroutines.launch


class HomeViewModel(
    private val remoteMoviesRepository: RemoteMovieRepository,
    private val localMoviesRepository: LocalMovieRepository
) : BaseViewModel() {

    /** */
    private val uiStateMLD = MutableLiveData<UIState<HomeDataModel>>()
    val uiState: LiveData<UIState<HomeDataModel>>
        get() = uiStateMLD

    /** */
    fun fetchMovies() {
        viewModelScope.launch {
            uiStateMLD.value = UIState.Loading(isLoading = true)
            fetchLatestMovies()
            fetchTopRatedMovies()
            uiStateMLD.value = UIState.Loading(isLoading = false)
        }
    }

    /** */
    private suspend fun fetchLatestMovies() {
        val latestMoviesResponse = remoteMoviesRepository.getLatest()
        val movies: List<MovieUIModel>?

        if (latestMoviesResponse is Response.Success) {
            movies = latestMoviesResponse.data.movies?.toUIModel()
            saveMoviesIntoDB(movies, MovieListType.Latest)
        } else {
            movies = localMoviesRepository.getLatestMovies()
        }

        val homeDataModel = HomeDataModel(movies, MovieListType.Latest)
        uiStateMLD.value = UIState.Success(homeDataModel)
    }

    private suspend fun fetchTopRatedMovies() {
        val topRatedResponse = remoteMoviesRepository.getTopRated()
        val movies: List<MovieUIModel>?

        if (topRatedResponse is Response.Success) {
            movies = topRatedResponse.data.movies?.toUIModel()
            saveMoviesIntoDB(movies, MovieListType.TopRated)

        } else {
            movies = localMoviesRepository.getRatedMovies()
        }

        val homeDataModel = HomeDataModel(movies, MovieListType.TopRated)
        uiStateMLD.value = UIState.Success(homeDataModel)

        // - get recommendations from TopRated movies
        val listOfIDs = movies?.take(7)?.map { it.id }
        fetchRecommendations(listOfIDs)

    }

    private suspend fun fetchRecommendations(movieIDs: List<String>?) {
        val remoteMovies = mutableListOf<MovieNetworkModel>()
        movieIDs?.forEach {
            val response = remoteMoviesRepository.getRecommendationsByMovieId(it)
            if (response is Response.Success) {
                remoteMovies.addAll(response.data.movies ?: emptyList())
            }
        }

        val uiMovies = if (remoteMovies.isNotEmpty()) {
            remoteMovies.distinctBy { it.id }.toUIModel()
        } else {
            localMoviesRepository.getRecommendedMovies()
        }

        saveMoviesIntoDB(uiMovies, MovieListType.Recommendation)
        val homeDataModel = HomeDataModel(
            uiMovies,
            MovieListType.Recommendation
        )

        uiStateMLD.value = UIState.Success(homeDataModel)
    }

    /** */
    private suspend fun saveMoviesIntoDB(movies: List<MovieUIModel>?, type: MovieListType) {
        movies?.forEach {
            val movieRoomModel = it.toRoomModel().apply {
                this.type = type.toString()
            }

            localMoviesRepository.insertMovie(movieRoomModel)
        }
    }


}