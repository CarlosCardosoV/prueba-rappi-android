package com.carloscardoso.themoviedb.modules.splash

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.carloscardoso.themoviedb.R
import com.carloscardoso.themoviedb.modules.home.HomeActivity

/** */
const val SPLASH_DURATION = 2200L

@SuppressLint("CustomSplashScreen")
class SplashActivity : AppCompatActivity() {

    /** */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        // -
        Handler(Looper.getMainLooper()).postDelayed({
            goToHomeActivity()
        }, SPLASH_DURATION)
    }

    /** */
    private fun goToHomeActivity() {
        startActivity(Intent(applicationContext, HomeActivity::class.java))
        finish()
    }
}