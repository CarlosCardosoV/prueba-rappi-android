# eMovie Prueba Android

Prueba técnica en Android que presenta una lista de películas obtenidas de la API de TheMovieDB. Se organizan en 3 secciones principales:

1. Agregadas recientemente (Latest)
2. En tendencia (Top Rated)
3. Recomendadas para ti (Recommended)

Se presenta una pantalla de detalle de la película con opción a ver su trailer


## Preguntas respondidas

1. **¿En qué consiste el principio de responsabilidad única? ¿Cuál es su propósito?** Este principio consiste en diseñar las clases o métodos de un programa de modo que tengan una sola funcionalidad concreta. El propósito del principio es tener un código mas legible y mantenible a lo largo del tiempo.

2. **¿Qué características tiene, según su opinión, un “buen” código o código limpio?** En mi opinión un código limpio es aquel que se apega a los principios SOLID, a una arquitectura bien definida, a los estándares del lenguaje. También cumple características como que tenga un buen 'naming' para las clases funciones y variables.  Lo anterior en general nos permite tener un código mas legible para otros desarrolladores y para nuestro yo futuro.

3. **Detalla cómo harías todo aquello que no hayas llegado a completar**
- Añadiría una Shared Element Transition para el flujo de la pantalla de home hacia el detalle.
- Usaria alguna biblioteca para presentar el trailer de la película dentro de la aplicación y no en YouTube.


## Descripción del proyecto

Se utilizó arquitectura MVVM para la aplicación, Kotlin como lenguaje de desarrollo, Retrofit como cliente HTTP, Room para almacenamiento de datos local, mockk para las pruebas unitarias.

## Capas y principales modulos de la aplicación

1. **network:** Establece la configuración del cliente para realizar peticiones http. Define el conjunto de servicios que se invocarán y contiene los modelos de respuesta de dichos servicios.
2. **persistence:**  Gestiona los elementos relacionados al almacenamiento local con room (la definición de la BD y los modelos room)
3. **repository:** Capa que abstrae el acceso a los datos locales o remotos.
4. **modules:** Define las distintas features de la aplicación (home, detail) donde cada una tiene asociada un ViewModel para la obtención y presentación de la data.
5. **di:** Define las distintas dependencias que se requiere a ser inyectadas.
6. **ui:** Contiene componente personalizados, extensiones de UI y modelos de la UI.


## Principales bibliotecas usadas en la aplicación

- [Lottie](https://github.com/airbnb/lottie-android) para presentar animaciones

- [Glide](https://github.com/bumptech/glide) como libreria para cargar las imagenes.

- [Paris](https://github.com/airbnb/paris) para establecer styles de forma programática

- [Retrofit](https://github.com/square/retrofit): cliente http para realizar las peticiones

- [Room](https://developer.android.com/training/data-storage/room): biblioteca para almacenamiento de datos locales

- [Koin](https://insert-koin.io): biblioteca para gestionar la inyección de dependencias

- [mockk](https://mockk.io): para pruebas unitarias






